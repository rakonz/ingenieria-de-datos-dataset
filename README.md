# Integrantes
    -Francisco Cid
    -Saúl Cifuentes
    -José Gutierrez
    -Héctor Sepúlveda

# Contexto

El presente conjunto de datos extraído desde la plataforma Kaggle, trata sobre accidentes de tránsito de todo el país de Estados Unidos y cubre 49 de los 50 estados. Los datos se recopilan continuamente desde febrero del 2016 a diciembre del 2021, utilizando varios proveedores de datos, incluidas varias API que brindan transmisión de datos de eventos de tráfico. Estas API transmiten eventos de tráfico capturados por una variedad de entidades, como los departamentos de transporte estatales y de EE. UU., agencias de aplicación de la ley, cámaras de tráfico y sensores de tráfico dentro de las redes de carreteras. Actualmente, hay alrededor de 1,5 millones de registros de accidentes en este conjunto de datos.


# Motivación

Como equipo se nos ofreció la oportunidad de trabajar con una organización estadounidense que se dedica a estudiar la severidad que impacta en el tráfico producido por accidentes automovilísitcos en aquel país. Se nos pidió realizar un análisis del set de datos que nos fue entregado, del cual se espera extraer información útil que pueda llegar a clasificar qué tan severo es cierto accidente, dependiendo de ciertos atributos. La idea del trabajo es encontrar relaciones distintas al impacto en el tráfico, la cual es la médida principal para catalogar la severidad en el dataset.
Esta clasificación cobra real importancia si pensamos en el manejo de los servicios de emergencias de aquel país (bomberos, equipo médico, policia y otros servicios), que como se sabe, son entidades que deben llegar lo antes posible a cierto lugar, y debido a la severidad de un accidente, puede que deban evitar la ruta donde ocurrió éste.


# DATASET US-ACCIDENTS
Página web Kaggle (https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents?datasetId=199387&sortBy=voteCount) 

